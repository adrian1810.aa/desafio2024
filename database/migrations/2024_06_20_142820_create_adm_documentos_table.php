<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('adm_documentos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('adm_persona_id');
            $table->foreign('adm_persona_id')
                  ->references('id')
                  ->on('adm_personas');
            $table->string('nuemero', 100);
            $table->boolean('es_prinipal')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('adm_documentos');
    }
};
