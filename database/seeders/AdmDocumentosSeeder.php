<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class AdmDocumentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $faker = Faker::create();
        // $personas = DB::table('adm_personas')->select('id')->get();
        // $data = [];
        // foreach ($personas as $persona) {
        //     $numDocumentos = $faker->numberBetween(1, 5);
        //     $documentosPrincipales = []; // Arreglo para almacenar los IDs de documentos principales

        //     for ($i = 0; $i < $numDocumentos; $i++) {
        //         $esPrincipal = false;
        //         // Asegurar que solo haya un documento principal por persona
        //         if ($i == 0) {
        //             $esPrincipal = true;
        //             $documentosPrincipales[] = $i + 1; // Almacenar el ID del documento principal
        //         }

        //         $data[] = [
        //             'adm_persona_id' => $persona->id,
        //             'numero' => $faker->unique()->numerify('##########'),
        //             'es_principal' => $esPrincipal,
        //             'created_at' => now(),
        //             'updated_at' => now(),
        //         ];
        //     }
        // }
        // foreach ($documentosPrincipales as $docPrincipalId) {
        //     DB::table('adm_documentos')
        //         ->where('id', $docPrincipalId)
        //         ->update(['es_principal' => true]);
        // }

        $faker = Faker::create();
        $personas = DB::table('adm_personas')->select('id')->get();
        $lote = 1000; // Tamaño del lote para inserciones
        $limite = 15000; // Número total de registros deseados
        $totalCount = 0; // Contador para mantener el número total de registros insertados

        $data = [];
        foreach ($personas as $persona) {
            // Generar un número aleatorio de documentos por persona (entre 1 y 5)
            $numDocumentos = $faker->numberBetween(1, 5);

            for ($i = 0; $i < $numDocumentos; $i++) {
                $data[] = [
                    'adm_persona_id' => $persona->id,
                    'numero' => $faker->unique()->numerify('##########'), // Generar un número de documento único
                    'es_principal' => ($i == 0), // Solo el primer documento será principal
                    'created_at' => now(),
                    'updated_at' => now(),
                ];

                $totalCount++; // Incrementar el contador total de registros

                // Insertar los registros por lotes
                if (count($data) >= $lote) {
                    DB::table('adm_documentos')->insert($data);
                    $data = []; // Reiniciar el array de datos
                }

                // Verificar si se alcanzó el número deseado de registros
                if ($totalCount >= $limite) {
                    break 2; // Salir de ambos bucles foreach y for
                }
            }
        }

        // Insertar los registros restantes que no completaron el último lote completo
        if (!empty($data)) {
            DB::table('adm_documentos')->insert($data);
        }
    }
}
