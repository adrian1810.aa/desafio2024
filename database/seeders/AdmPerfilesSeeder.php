<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class AdmPerfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        $usuarios = DB::table('usuarios')->select('id')->get();
        $roles = DB::table('adm_roles')->select('id')->get();

        $data = [];
        foreach ($usuarios as $usuario) {
            $numPerfiles = $faker->numberBetween(1, 3);

            for ($i = 0; $i < $numPerfiles; $i++) {
                $rolId = $roles->random()->id; 
                
                $data[] = [
                    'adm_rol_id' => $rolId,
                    'user_id' => $usuario->id,
                    'activo' => $faker->boolean(),
                    'descripcion' => $faker->sentence(),
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }

        DB::table('adm_perfiles')->insert($data);
    }
}
