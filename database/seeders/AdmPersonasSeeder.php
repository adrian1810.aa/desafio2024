<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class AdmPersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        $data = [];
        for ($i = 0; $i < 10000; $i++) {
            $data[] = [
                'nombre' => $faker->firstName,
                'apellido' => $faker->lastName,
                'sexo' => $faker->randomElement(['M', 'F']),
                'fecha_nacimiento' => $faker->date(),
                'created_at' => now(),
                'updated_at' => now(),
            ];

            if ($i > 0 && $i % 1000 == 0) {
                DB::table('adm_personas')->insert($data);
                $data = [];
            }
        }

        if (!empty($data)) {
            DB::table('adm_personas')->insert($data);
        }
        
    }
}
