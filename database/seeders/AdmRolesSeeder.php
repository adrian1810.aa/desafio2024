<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class AdmRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        
        $faker = Faker::create();

        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $data[] = [
                'nombre' => $faker->unique()->word, 
                'codigo' => strtoupper($faker->unique()->lexify('???')),
                'activo' => $faker->boolean(),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        DB::table('adm_roles')->insert($data);
    }
}
