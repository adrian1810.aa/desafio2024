<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();
        $personas = DB::table('adm_personas')->select('id')->limit(100)->get();
        $batchSize = 1000; // Tamaño del lote para inserciones

        $data = [];
        foreach ($personas as $persona) {
            $data[] = [
                'adm_persona_id' => $persona->id,
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'password' => bcrypt('password123'), // Encriptar una contraseña fija
                'activo' => $faker->boolean(90), // 90% de probabilidad de estar activo
                'bloqueado' => $faker->boolean(10), // 10% de probabilidad de estar bloqueado
                'created_at' => now(),
                'updated_at' => now(),
            ];

            // Insertar los registros por lotes
            if (count($data) >= $batchSize) {
                DB::table('usuarios')->insert($data);
                $data = []; // Reiniciar el array de datos
            }
        }

        // Insertar los registros restantes que no completaron el último lote completo
        if (!empty($data)) {
            DB::table('usuarios')->insert($data);
        }
    }
}
