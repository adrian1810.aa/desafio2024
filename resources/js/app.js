// import './bootstrap';
// import Vue from 'vue';
// import App from './components/App.vue';


// new Vue({
//     render: h => h(App),
// }).$mount('#app');


import { createApp } from 'vue';
import App from './components/App.vue';
import axios from 'axios';
axios.defaults.baseURL = 'http://localhost:8000/api';

createApp(App).mount('#app');
